﻿using System;

namespace Berseneva.Lab4.Exercise2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите n:");
            double n = Convert.ToDouble(Console.ReadLine());
            double k;
           
            k = 1;
            Hut(k);
            static double Hut(double k)
            {
                double result = (k + 1) / 3.0;
                return result;
            }
            static double MyFunctionRecursionSum(double k) => (k == 1) ? Hut(1) : Hut(k) + MyFunctionRecursionSum(k - 1);
            Console.WriteLine($"f({n})=" + MyFunctionRecursionSum(n));
            Console.ReadLine();

                
        }
    }
}
