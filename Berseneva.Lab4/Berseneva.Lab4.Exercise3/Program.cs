﻿using System;

namespace Berseneva.Lab4.Exercise3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите n:");
            double n = Convert.ToDouble(Console.ReadLine());
            double k;
           
            k = 1;
            Cut(k);
            static double Cut(double k)
            {
                double result = (k + 1) / 3.0;
                return result;
            }
            static double MyFunctionRecursionProd(double k) => (k == 1) ? Cut(1) : Cut(k) * MyFunctionRecursionProd(k - 1);
            Console.WriteLine($"f({n})=" + MyFunctionRecursionProd(n));
            Console.ReadLine();
        }
    }
}
